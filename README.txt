Hola morro, este es mi app de django, use un simple wagtail.

Te preguntaras que hice para que funcionara con PSQL y no con pslite no?
Bueno aqui tenemos una pequeña explicacion..

1. Se debio de crear una aplicacion de wagtail, para eso instalamos wagtail por medio de "pip3 install wagtail"
    una vez instalado se creara un proyecto vacio de wagtail, con un Dockerfile, el cual desecharemos. y crearemos uno desde cero.

2. Wagtail por default utiliza psqlite, queremos cambiarlo para que utilice PSQL. Editaremos el archivo "app/settings/base.py"
    Y eliminaremos el bloque referente a `database`
3. Crearemos el archivo "app/setting/dev.py" introduciremos nuestras variables de entorno. (este archivo es un template ) se usara la misma estructura del bloque que borramos anteriormente

from .base import *

SECRET_KEY = os.environ.get("SECRET_KEY")
DEBUG = os.environ.get("DEBUG")
ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")

# Database PostgreSQL
DATABASES = {
    'default': {
        'ENGINE': os.environ.get("SQL_ENGINE"),
        'NAME': os.environ.get("SQL_DATABASE"),
        'USER': os.environ.get("SQL_USER"),
        'PASSWORD': os.environ.get("SQL_PASSWORD"),
        'HOST': os.environ.get("SQL_HOST"),
        'PORT': os.environ.get("SQL_PORT"),
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    from .local import *
except ImportError:
    pass


4. Docker file, se usara una imagen de python en alpine, (las imagenes de alpine pesan menos, pero una vez construid la imagen la verdad pudierra llegar a pesar casi lo mismo, equisde) e instalaremos las librerias necesarias de python, psql
postgresql-dev,gcc,python3-dev,musl-dev,libffi-dev,openssl-dev,jpeg-dev,libwebp-dev,zlib-dev,freetype-dev,lcms2-dev,openjpeg-dev,tiff-dev,tk-dev,tcl-dev,libxml2-dev,libxslt-dev,libxml2.

5. Importante una vez instaladas las librerias upgradear (apk upgrade)

6. Agregamos un entry point que correra el comando "nz -z host port" este revisara si es posible coenctarse al PSQL server, una vez hecho esto, correra los scripts de inicio de app y db migrate.

7. No menos importante, a inicio de docker file definir las variables de ambiente que usara nuestro archivo "dev.py" estas se pasan asi:
    ARG VARIABLE_X
    ENV HOST=$VARIABLE_X

    Importante, al momento de hacer el build de la imagen, construir con "--build-arg ARG_A_USAR=VALOR_RANDOM_QUE_NO_ES_RANDOM"
    Las variables necesarias se expecifican en el Dockerfile, a exepcion de la contraseña $SQL_PASSWORD, esta se paso como secreto por medio de AWS.




                                        TERRAFORM Info.

Terraform alch si batalle, por suerte si tiene mucha docuemntacion, muchas servicios fueron a prueba y error,
1. Principalmente, se debe tener un archivo llamado "terraform.tfvars" con el siguiente contenido
    aws_access_key  = "Tu access key"
    aws_secret_key  = "Tu secret Key"
    ami_ecs         = "ami with amazon linux, varia segun region, o algo asi"
    user_key        = "key pem"
    certificate_acm = "Certificado ACM de tu dominio"
    my_secret       = "Nombre del secreto, en mi caso es 'SQL_PASSWORD'"
    my_dummy_secret = "acr completo del secreto"

2. El otro archivo de variables se llama "variable.tf"
    Aqui no hay mucho que hacer, son variables de ... nombres de servicios, num de puertos, algo fancy para que el codigo se vea mas bonito.

3. Hacemos un terraform init, no recuerdo si es antes o despues de crear algun archivo de terraform.

4. Empecemos con el codigo. La verdad no importa en que orden los hagamos. Los despedace en muchos archivos.
    NOTA: Tuve que hacer varias cosas manuales, y despues borrarlas. Para saber que era lo necesario de meter en el bloque del servicio de terraform
    vpc.tf (creamos VPC, subnets, route tables, NAT gt)
    alb-tg.tf (creamos un alb, tg y configuramos los listener)
    asg.tf (creamos un ASG)
    cloudwatch.tf (creamos unas alarmas)
    ecs.tf (creamos un cluster)
    iam.tf (creamos unos roles con sus politicas adecuadas)
    security-groups.tf (creamos SG para las instancias de ASG, el ALB, y DB)
    sns.tf (creamos un SNS topic y hacemos una subscripcion)
    variable.tf (variables no delicadas)
    terraform.tfvars (variables delicadas)

5. una vez todo eso hacemos un "terraform plan" si no nos arroja algun error proseguimos con...

6. terraform apply


                            ESCANER DE IMAGENES

La verdad tenia mis dudas sobre como implementar esta wea. Pero volviendo a la mentalidad que en si, la pipeline corre en un "server" busque herramientas para escaneo de seguridad en imagenes. Opte por la herramienta de trivy, que la encontre sencilla de configurar y juraria que lovato me comento tmbien de trivy.
1. Lo primero que hice fue descargar Trivy justo despues de buildear la imagen, y confirmar que funcionara con un `trivy --version`

2. Una vez verificado la instalacion, realice un escaneo, sin que este realizara alguna accion, solamente escaneo.
        - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b /usr/local/bin
        - trivy image $REPOSITORY_URL:latest

3. Una vez verificado el escaneo, y que este me arrojo varias vulnerabilidades. Agregue una nueva linea. Donde especifico que hay una vulnerabilidad critica, se rompe la pipeline. Si no es asi se prosigue con el push al ECR.
        - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b /usr/local/bin
        - trivy --no-progress --output scanning-report.json $REPOSITORY_URL:latest
        - trivy --exit-code 1 --no-progress --severity CRITICAL $REPOSITORY_URL:latest
