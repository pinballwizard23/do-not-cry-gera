# /app/Dockerfile
# Pull the base image
FROM python:3.8.5-alpine3.12

# Set workdirectory
WORKDIR /usr/src/app

ARG S_KEY
ARG ALLOWED_HOSTS
ARG DB_ENGINE
ARG DB_NAME
ARG DB_USER
ARG DB_HOST
ARG DB_PORT
ARG DB

ENV DEBUG=True
ENV SECRET_KEY=${S_KEY}
ENV DJANGO_ALLOWED_HOSTS=${ALLOWED_HOSTS}
ENV SQL_ENGINE=${DB_ENGINE}
ENV SQL_DATABASE=${DB_NAME}
ENV SQL_USER=${DB_USER}
ENV SQL_HOST=${DB_HOST}
ENV SQL_PORT=${DB_PORT}
ENV DATABASE=${DB}

# Enviroment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install server packages
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev libffi-dev openssl-dev \
    && apk add jpeg-dev libwebp-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev libxml2-dev libxslt-dev libxml2


# Install python packages
RUN pip install --upgrade pip
COPY . .
RUN pip install -r requirements.txt

# Postgres Entrypoint
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
